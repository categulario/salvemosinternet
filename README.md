# ¡Queremos redes libres!

Repositorio del _fanzine_ _¡Queremos redes libres! Por otras
telecomunicaciones posibles_.

# Compilación

Para producir todos los formatos a partir del archivo MD y para actualizar el sitio solo ejecuta:

```
./build
```

Requiere:

* [Pecas](https://pecas.perrotuerto.blog/)
* [`export-pdf`](https://gitlab.com/snippets/1917490)
* `pdftk` (para añadir portada a PDF)
* Ruby (dependencia de Pecas)
* Pandoc (dependencia de `export-pdf`)
* `lualatex` (dependencia de `export-pdf`)
* `pdfbook2` (dependencia de `export-pdf`)

# Recursos primigenios

Los materiales base para esta edición son:

* _¡Queremos redes libres!_, [publicado por el Partido Pirata de Argentina](https://partidopirata.com.ar/2018/01/16/queremos-redes-libres).
* «¿Cuáles son las amenazas?», [publicado por la campaña mexicana #SalvemosInternet](https://salvemosinternet.mx/conoce-mas).

# Licencia

Este _fanzine_ tiene [Licencia Editorial Abierta y Librea (LEAL)](https://leal.perrotuerto.blog).

Con LEAL eres libre de usar, copiar, reeditar, modificar, distribuir
o comercializar bajo las siguientes condiciones:

* Los productos derivados o modificados han de heredar algún tipo de LEAL.
* Los archivos editables y finales habrán de ser de acceso público.
* El contenido no puede implicar difamación, explotación o vigilancia.
