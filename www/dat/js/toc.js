window.onload = function () {
  tocbot.init({
    tocSelector: '.js-toc',
    contentSelector: '.content',
    headingSelector: 'h1, h2, h3',
    hasInnerContainers: true,
    collapseDepth: 0,
    scrollSmooth: true,
  });
}
